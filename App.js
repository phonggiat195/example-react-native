import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {createStackNavigator, createAppContainer} from 'react-navigation'
import MainTabs from './navigation/MainTabs'
import Detail from './navigation/Detail'
import HomeScreen from './screens/HomeScreen'
import HotNewScreen from './screens/HotNewScreen'
import DailyNewScreen from './screens/DailyNewScreen'

const Application = createStackNavigator ({
    Main:{
      screen: MainTabs,
    },
    Details: {
      screen: Detail,
      },
    'Trang Chủ' : {
      screen: HomeScreen,
      },
    'Tin Mới' : {
      screen: DailyNewScreen,
        },
    'Tin Hot' : {
      screen: HotNewScreen
        }

  })
const AppContainer = createAppContainer(Application);

export default class App extends React.Component {
  render() {
    return (
      
        <AppContainer/>
      
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
