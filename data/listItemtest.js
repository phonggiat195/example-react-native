import React, { Component } from 'react'
import {View, FlatList, StyleSheet, Text, Image } from 'react-native'


class FlatListItem extends Component{
    constructor (props) {
        super(props)
        this.state = {
          input: ''
        }
     }
    render(){
        return (
            <View style = {{
                flex: 1,
                flexDirection: 'row',         
                backgroundColor: this.props.index % 2 == 0 ? 'mediumseagreen' : 'tomato' 
            }}>
            <Image source= {{uri: this.props.item.imageUrl}}
                    style = {{width: 100, height: 100, margin: 5}}
            ></Image>

            <View>
                <Text style = {styles.itemList}>{this.props.item.name}</Text>
                <Text style = {styles.itemList}>{this.props.item.introduce}</Text>
            </View>
                
            </View>
        );
    }
}
export default FlatListItem;

const styles = StyleSheet.create({
    itemList : {
        color: 'white',
        padding: 10,
        fontSize: 16,
    }
});