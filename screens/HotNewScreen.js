import React, { Component } from 'react'
import {View, FlatList, StyleSheet, Text, TouchableOpacity } from 'react-native'
import flatListData from '../data/flatListData'
import FlatListItem from '../data/listItemtest'

export default class HotNewScreen extends Component {
    static navigationOptions ={
        header: null
    }
    
    render() {
        return (
            <View style={{flex:1, marginBottom: 10}}>
                <FlatList
                    data = {flatListData.slice(2, 5)}
                    renderItem = {({item, index}) =>{
                        return (
                            <TouchableOpacity onPress={() => 
                        this.props.screenProps.navi(item)}>
                            
                            <FlatListItem item= {item} index = {index}>
                            </FlatListItem>
                            </TouchableOpacity>
                        )
                    }}
                >
                </FlatList>
            </View>
        )
    }
}
