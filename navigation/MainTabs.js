import React, { Component } from 'react'
import {createAppContainer, createStackNavigator, createBottomTabNavigator} from 'react-navigation'
import {View, StatusBar, StyleSheet} from 'react-native'
import HomeScreen from '../screens/HomeScreen'
import DailyNewScreen from '../screens/DailyNewScreen'
import HotNewScreen from '../screens/HotNewScreen'

export const MyStackNavigator = createStackNavigator({
    'Trang Chủ':{
        screen: HomeScreen,
    },
});

const ApplicationsNews = createBottomTabNavigator({
    'Trang Chủ' : MyStackNavigator,
    'Tin Mới': DailyNewScreen,
    'Tin Hot': HotNewScreen,
})

const AppContainer = createAppContainer(ApplicationsNews);

export default class MainTabs extends Component {
    static navigationOptions ={
        header: null
    }

    render() {
        return (
            <View style={styles.container}>
                <AppContainer screenProps={{navi:(item) =>this.props.navigation.navigate('Details', {
                    item: item
                })}}/>

            </View>
        )
    }
}
const statusBarHeight = StatusBar.currentHeight;

const styles = StyleSheet.create({
    container: {
        flex: 1,     
        backgroundColor: '#F5FCFF',
        marginBottom: statusBarHeight,
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});

