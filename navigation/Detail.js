import React, { Component } from 'react'
import { View, KeyboardAvoidingView, Dimensions, Text, Image, ScrollView} from 'react-native'


export default class Detail extends Component {

    constructor(props){
        super(props)
        this.state = {
          item: {}
        }
    }

    componentDidMount() {
      this.setState({
        item: this.props.navigation.state.params.item
      })
      console.log(this.state.item)
    }

    componentWillReceiveProps(props) {
      
    }

    render() {
        const { item } = this.state
        if(item) {
          return (
              <View style= {{flex: 1}}>
                      <ScrollView>
                      <Image
    
                          style={{width: deviceWidth,
                          height:imageHeight}} 
                          source={{uri: item.imageUrl}}/>

              <View 
                style={{flexDirection:'column',
                  }}>

                <Text>{item.name}</Text>

                <Text>{item.introduce}</Text>
              </View>
                      </ScrollView>
              </View>
          )
      }
    }
}
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;
const imageHeight = deviceHeight * 24 /100;
